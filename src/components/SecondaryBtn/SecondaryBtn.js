import React from "react";

import "./SecondaryBtn.css";
import {Icon} from "semantic-ui-react";

const SecondaryBtn = props => {
    return (
        <div>
            <button className={"secondary-btn"}>
                <Icon name={"phone"} />{props.children}</button>
        </div>
    )
};

export default SecondaryBtn;

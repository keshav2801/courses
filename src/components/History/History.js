import React from "react";

import "./History.css";
import {Icon} from "semantic-ui-react";

const History = props => {
    return (
        <div className={"history-container"}>
            <div className={"upper"}>
                <div className={"img-container"}>
                    <img className={"image"} alt={"img"} />
                </div>
                <div className={"content"}>
                    <div className={"inner-container"}>
                        <div className={"head-text"}>
                            <h4>Advance Track for Sr. Programmer</h4>
                            <span className={"price strike old-price"}>Rs. 3,250</span>
                        </div>
                        <div className={"sub-text"}>
                            <span>Order ID: 5995030209928   • Ordered on: 28-08-2020</span>
                            <span className={"price new-price"}>Rs. 2,400</span>
                        </div>
                    </div>
                </div>
            </div>

            <div className={"upper-mobile"}>
                <div className={"top"}>
                    <div className={"img-container"}>
                        <img className={"image"} alt={"img"} />
                        <h4>Advance Track for Sr. Programmer</h4>
                    </div>
                    <div>
                        <Icon name={"chevron right"}/>
                    </div>
                </div>
                <div className={"mid"}>
                    <div>
                        <span className={"price strike old-price"}>Rs. 3,250</span>
                        <span className={"price new-price"}>Rs. 2,400</span>
                    </div>
                    <div>
                        <span><Icon className={"status " + props.status} style={{float: 'left', lineHeight: 1.7}} name={props.status === 'complete' ? 'check' : (props.status === 'failed' ? 'close' : 'redo')}/></span>
                        <span style={{marginLeft: 10}}>{props.status}</span>
                    </div>
                </div>
                <div className={"low"}>
                    <div>
                        <p>Order ID: 5995030209928</p>
                    </div>
                    <div>
                        <p>Purchased on: 28-08-2020</p>
                    </div>
                </div>
            </div>

            <div className={"lower"}>
                <div>
                    <div className={"lower-links"}>
                        <a>View Order Details</a>
                        <a>Help</a>
                        <a>Reorder</a>
                    </div>
                </div>
                <div className={"status " + props.status}>
                    <span><Icon className={"status " + props.status} style={{float: 'left', lineHeight: 1.7}} name={props.status === 'complete' ? 'check' : (props.status === 'failed' ? 'close' : 'redo')}/></span>
                    <span style={{marginLeft: 10}}>{props.status}</span>
                </div>
            </div>
        </div>
    )
};

export default History;

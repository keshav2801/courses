import React from "react";

import "./PrimaryBtn.css";

const PrimaryBtn = props => {
    const {btnClass} = props;

    return (
        <button
            className={btnClass}
            {...props}>{props.children}
        </button>
    );
};

export default PrimaryBtn;

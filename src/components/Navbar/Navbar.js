import React from "react";
import {Menu} from "semantic-ui-react";
import {Navbar, Form, Nav, Button, FormControl} from "react-bootstrap";

import "./Navbar.css";
import Logo from "../Logo/Logo";
import SecondaryBtn from "../SecondaryBtn/SecondaryBtn";

const NavBar = props => {
    return (
        <div className={"navbar-container navbar-mobile"}>
            <div className={"nav-items"}>
                <div className={"left"}>
                    <div className={"logo-container"}>
                        <Logo/>
                    </div>
                    <div className={"nav-links active"}>
                        <a>Home</a>
                    </div>
                    <div className={"nav-links"}>
                        <a>Tracks</a>
                    </div>
                    <div className={"nav-links"}>
                        <a>Courses</a>
                    </div>
                    <div className={"nav-links"}>
                        <a>About</a>
                    </div>
                </div>
                <div className={"right"}>
                    <div className={"nav-links"}>
                        <SecondaryBtn>Request a callback</SecondaryBtn>
                    </div>
                    <div className={"nav-links"}>
                        <span className={"avatar"}>JB</span>
                    </div>
                </div>
            </div>
            <div className={"nav-items-mobile"}>
                <div>
                    <Logo/>
                </div>
                <div>
                    <SecondaryBtn>Login/Signup</SecondaryBtn>
                </div>
            </div>
        </div>
    )
};

export default NavBar;

import React from "react";

import "./Logo.css";
import {Icon} from "semantic-ui-react";

const Logo = props => {
    return (
        <React.Fragment>
            <span className={"icon-hamburger"}>
                <Icon name={"align justify"}/>
            </span>
            <span className={"course-title"}>Courses</span>
        </React.Fragment>


    )
};

export default Logo;

import React, {Component} from "react";
import {Menu, Grid, Icon} from "semantic-ui-react";

import "./SideBar.css";

class SideBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {onTabSelect, currentIndex} = this.props;
        return (
            <div>
                <h3>Account Settings</h3>
                <ul>
                    <li className={currentIndex === 1 ? "active" : ""} onClick={() => onTabSelect(1)}>
                        <span>
                            <Icon name={"user outline"} />
                        </span>
                        <span className={"menu-text"}>Profile Details</span>
                    </li>
                    <li className={currentIndex === 2 ? "active" : ""} onClick={() => onTabSelect(2)}>
                        <span>
                            <Icon name={"history"} />
                        </span>
                        <span className={"menu-text"}>Order Details</span>
                    </li>
                </ul>
            </div>
        )
    }
}

export default SideBar;

import React, {Component, useState} from "react";
import {Form} from "semantic-ui-react";
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'

import "./ProfileDetails.css";
import PrimaryBtn from "../../components/PrimaryBtn/PrimaryBtn";

class ProfileDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handlePhoneChange = () => {
        console.log("phone");
    };

    render() {
        return (
            <div className={"profile-detail-form"}>
                <Form>
                    <div className={"align-input"}>
                        <Form.Field>
                            <label>First Name</label>
                            <input placeholder='First Name' />
                        </Form.Field>
                        <Form.Field>
                            <label>Last Name</label>
                            <input placeholder='Last Name' />
                        </Form.Field>
                    </div>
                    <div className={"align-input second"}>
                        <Form.Field>
                            <label>Email Address</label>
                            <input placeholder='Last Name' />
                        </Form.Field>
                        <Form.Field>
                            <label>Mobile Number</label>
                            <PhoneInput
                                placeholder="Enter phone number"
                                onChange={() => this.handlePhoneChange()}
                                defaultCountry={"IN"}/>
                        </Form.Field>
                    </div>
                    <div className={"btn-fields"}>
                        <div>
                            <PrimaryBtn btnClass={"save-btn"}>Save</PrimaryBtn>
                        </div>
                        <div>
                            <button className={"update-pwd-btn"}>Update Password</button>
                        </div>
                    </div>
                </Form>
            </div>
        )
    }
}

export default ProfileDetails;

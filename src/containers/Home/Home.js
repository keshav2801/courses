import React, {Component} from "react";

import "./Home.css";
import SideBar from "../SideBar/SideBar";
import ProfileDetails from "../ProfileDetails/ProfileDetails";
import OrderHistory from "../OrderHistory/OrderHistory";
import History from "../../components/History/History";
import {Icon} from "semantic-ui-react";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentIndex: 1
        }
    }

    updateDimensions = () => {
        if (this.state.currentIndex === 0 && window.innerWidth > 768) {
            this.setState({currentIndex: 1});
        }
    };

    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    onTabSelect = index => {
        this.setState({
            currentIndex: index
        });
    };

    render() {
        const {currentIndex} = this.state;
        return (
            <React.Fragment>
                {currentIndex === 1 && <div className={"backdrop"} onClick={() => this.onTabSelect(0)}></div>}
                <div className={"home-container"}>
                    <div className={"sidebar-outer-container"}>
                        <SideBar
                            currentIndex={currentIndex}
                            onTabSelect={this.onTabSelect}/>
                    </div>
                    <div className={"main-content-outer-container"}>
                        {currentIndex === 1 && <ProfileDetails/>}
                        {currentIndex === 2 && <OrderHistory/>}
                    </div>
                </div>
                <div className={"home-container-mobile"}>
                    <h3 className={"header-mobile"}>Account Settings</h3>
                    <div className={"menu"}>
                        <div className={"menu-item"} onClick={() => this.onTabSelect(1)}>
                            <h5><Icon name={"user outline"}/><span style={{marginLeft: 16}}>Profile Details</span></h5>
                            <p>Personal info, change password</p>
                        </div>
                    </div>
                    <h3 className={"mobile-heading-text"}>Order History</h3>
                    <div>
                        <History status={"failed"}/>
                        <History status={"pending"}/>
                    </div>
                    {currentIndex === 1 && (
                        <div className={"fixed-container"}>
                            <div className={"fixed-header"}>
                                <h3 className={"fixed-heading"}>Profile Details</h3>
                                <Icon name={"close"} onClick={() => this.onTabSelect(0)} />
                            </div>
                            <ProfileDetails/>
                        </div>
                    )}
                </div>
            </React.Fragment>
        )
    }
}

export default Home;

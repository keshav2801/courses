import React, {Component} from "react";

import "./OrderHistory.css";
import History from "../../components/History/History";

class OrderHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <History status={"failed"}/>
                <History status={"pending"}/>
                <History status={"complete"}/>
            </React.Fragment>
        )
    }
}

export default OrderHistory;

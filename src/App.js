import React, {Component} from 'react';

import './App.css';
import Home from "./containers/Home/Home";
import NavBar from "./components/Navbar/Navbar";

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={"app-container"}>
                <NavBar/>
                <Home/>
            </div>
        )
    }
}

export default App;
